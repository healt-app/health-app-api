// mandatory
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
// file uploader
const path = require('path');
const multer = require('multer');
const logger = require('morgan');
const serveIndex = require('serve-index')
// firebase
const fireBaseRoute = require('./app/routes/firebase.route');
const config = require('./app/config/config');

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
});

//will be using this for uplading
const upload = multer({ storage: storage });


app.use(logger('tiny'));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}));

//app.use(express.static('public'));
app.use('/ftp', express.static('public'), serveIndex('public', { 'icons': true }));

// firebase route
app.use('/firebase', fireBaseRoute);

var routes = require('./app/routes/routes');
routes(app, upload);

app.listen(config.port, () => console.log('Node js Restfull API server started on: ' + config.port));