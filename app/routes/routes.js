'use strict';
const debug = require('debug')('myapp:server');

module.exports = function (app, upload) {
    var api = require('./../controllers/controller');

    app.route('/')
        .get(api.index);

    app.post('/fileUploader', upload.single('file'), function (req, res) {
        debug(req.file);
        api.uploadComplete(req.file, res)
    })

    // image route
    app.route('/list-image')
        .get(api.getListImage);

    app.route('/menu')
        .get(api.getMenu);

    app.route('/menu')
        .post(api.addMenu);

    app.route('/menu')
        .put(api.editMenu);

    app.route('/menu')
        .delete(api.deleteMenu);

    app.route('/list-menu')
        .get(api.getListMenu);

    app.route('/detail-menu')
        .get(api.getDetailMenu);

    app.route('/video')
        .get(api.getVideo);

    app.route('/video')
        .post(api.addVideo);

    app.route('/video')
        .put(api.editVideo);

    app.route('/video')
        .delete(api.deleteVideo);

    app.route('/profile')
        .get(api.getProfile);

    app.route('/setting')
        .get(api.getSetting);

    app.route('/setting')
        .put(api.editSetting);

    app.route('/version')
        .get(api.getVersion);

};