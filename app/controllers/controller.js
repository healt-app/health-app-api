'use strict';
const response = require('./response');
var con = require('../config/conections');

function updateVersion() {
    con.query('UPDATE data_version SET version = (data_version.version + 1) WHERE id_data = 1',
        function (error, rows, fields) {
            if (error) {
                console.log(error)
                console.log('update version failed')
            } else {
                console.log('update version done')
            }
        });
}

exports.index = function (req, res) {
    response.ok('Welcome to HealtApp API', res)
};

exports.getVersion = function (req, res) {
    con.query('SELECT * FROM data_version WHERE id_data = 1', function (error, rows, fields) {
        if (error) {
            response.error("Internal Server Error", res)
        } else {
            response.ok(rows[0], res)
        }
    });
};

exports.uploadComplete = function (payload, res) {
    con.query('INSERT INTO image (image,original_name) values (?,?)',
        [payload.filename, payload.originalname],
        function (error, rows, fields) {
            if (error) {
                response.error('ohh... snap unable to save file', res)
            } else {
                response.ok(payload, res)
            }
        });
};

// image controller
exports.getListImage = function (req, res) {
    con.query('SELECT * FROM image ORDER BY id_image', function (error, rows, fields) {
        if (error) {
            response.error("Internal Server Error", res)
        } else {
            response.ok(rows, res)
        }
    });
}

// end

exports.getListMenu = function (req, res) {
    con.query('SELECT id_menu,title,image_menu FROM menu WHERE menu.title != "<profile>" ORDER BY id_menu', function (error, rows, fields) {
        if (error) {
            response.error("Internal Server Error", res)
        } else {
            response.ok(rows, res)
        }
    });
}

exports.getDetailMenu = function (req, res) {
    var idMenu = req.query.idMenu;
    con.query('SELECT * FROM menu WHERE id_menu = ?',
        [idMenu],
        function (error, rows, fields) {
            if (error) {
                response.error("Internal Server Error", res)
            } else {
                response.ok(rows, res)
            }
        });
}

exports.getMenu = function (req, res) {
    con.query('SELECT * FROM menu WHERE menu.title != "<profile>" ORDER BY id_menu', function (error, rows, fields) {
        if (error) {
            response.error("Internal Server Error", res)
        } else {
            response.ok(rows, res)
        }
    });
}

exports.addMenu = function (req, res) {
    var title = req.body.title;
    var imageMenu = req.body.imageMenu;
    var contentMenu = req.body.contentMenu;
    con.query('INSERT INTO menu (title,image_menu,content_menu) values (?,?,?)',
        [title, imageMenu, contentMenu],
        function (error, rows, fields) {
            if (error) {
                response.error(error, res)
            } else {
                response.ok(rows, res)
                updateVersion()
            }
        });
}

exports.editMenu = function (req, res) {
    var id = req.body.idMenu;
    var title = req.body.title;
    var imageMenu = req.body.imageMenu;
    var contentMenu = req.body.contentMenu;

    con.query('UPDATE menu SET title = ?,image_menu = ?,content_menu = ? WHERE id_menu = ?',
        [title, imageMenu, contentMenu, id],
        function (error, rows, fields) {
            if (error) {
                response.error(error, res)
            } else {
                response.ok(rows, res)
                updateVersion()
            }
        });
};

exports.deleteMenu = function (req, res) {
    var id = req.query.idMenu;

    con.query('DELETE FROM menu WHERE id_menu = ?',
        [id],
        function (error, rows, fields) {
            if (error) {
                response.error(error, res)
            } else {
                response.ok(rows, res)
                updateVersion()
            }
        });
};

// SELECT * FROM menu WHERE title = '<profile>'
exports.getProfile = function (req, res) {
    con.query('SELECT menu.image_menu,menu.content_menu,menu.id_menu FROM menu WHERE title = "<profile>"', function (error, rows, fields) {
        if (error) {
            response.error("Internal Server Error", res)
        } else {
            response.ok(rows[0], res)
        }
    });
}

exports.getVideo = function (req, res) {
    con.query('SELECT * FROM video ORDER BY id', function (error, rows, fields) {
        if (error) {
            response.error("Internal Server Error", res)
        } else {
            response.ok(rows, res)
        }
    });
}

exports.addVideo = function (req, res) {
    var title = req.body.title;
    var description = req.body.description;
    var videoImage = req.body.videoImage;
    var videoLink = req.body.videoLink;
    con.query('INSERT INTO video (title,description,video_image,video_link) values (?,?,?,?)',
        [title, description, videoImage, videoLink],
        function (error, rows, fields) {
            if (error) {
                response.error(error, res)
            } else {
                response.ok(rows, res)
                updateVersion()
            }
        });
}

exports.editVideo = function (req, res) {
    var id = req.body.idVideo;
    var title = req.body.title;
    var description = req.body.description;
    var videoImage = req.body.videoImage;
    var videoLink = req.body.videoLink;

    con.query('UPDATE video SET title = ?,description = ?,video_image = ?,video_link = ? WHERE id = ?',
        [title, description, videoImage, videoLink, id],
        function (error, rows, fields) {
            if (error) {
                response.error(error, res)
            } else {
                response.ok(rows, res)
                updateVersion()
            }
        });
};

exports.deleteVideo = function (req, res) {
    var id = req.query.idVideo;
    con.query('DELETE FROM video WHERE id=?',
        [id],
        function (error, rows, fields) {
            if (error) {
                response.error(error, res)
            } else {
                response.ok(rows, res)
                updateVersion()
            }
        });
};

exports.getSetting = function (req, res) {
    con.query('SELECT * FROM setting WHERE id_setting = 1', function (error, rows, fields) {
        if (error) {
            response.error("Internal Server Error", res)
        } else {
            response.ok(rows[0], res)
        }
    });
}

exports.editSetting = function (req, res) {
    var bannerImage = req.body.bannerImage;

    con.query('UPDATE setting SET banner_image = ? WHERE id_setting = 1',
        [bannerImage],
        function (error, rows, fields) {
            if (error) {
                response.error(error, res)
            } else {
                response.ok(rows, res)
                updateVersion()
            }
        });
};

