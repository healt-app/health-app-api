'use strict';

exports.ok = function(payload, res) {
  var data = {
      'status': 200,
      'payload': payload
  };
  res.json(data);
  res.end();
};

exports.notFound = function(payload, res) {
  var data = {
      'status': 404,
      'payload': payload
  };
  res.json(data);
  res.end();
};

exports.error = function(payload, res) {
  var data = {
      'status': 500,
      'payload': payload
  };
  res.json(data);
  res.end();
};