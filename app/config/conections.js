var mysql = require('mysql');
var util = require('util')

var db_config = {
  connectionLimit: 10,
  host: "37.59.55.185",
  port: "3306",
  user: "ZzjjhWpoDA",
  password: "UsvXfhW914",
  database: "ZzjjhWpoDA"
};

var pool;

pool = mysql.createPool(db_config);
pool.getConnection((err, connection) => {
  if (err) {
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      console.error('Database connection was closed.')
    }
    if (err.code === 'ER_CON_COUNT_ERROR') {
      console.error('Database has too many connections.')
    }
    if (err.code === 'ECONNREFUSED') {
      console.error('Database connection was refused.')
    }
  } else {
    console.log('Database connected')
  }
  if (connection) connection.release()
  return
})

pool.query = util.promisify(pool.query)

module.exports = pool;