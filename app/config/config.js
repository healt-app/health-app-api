module.exports = {
    port: process.env.PORT || 4000,
    fireBasePrivateKeyPath: '../config/healt-app-firebase.json',
    firebaseStorageBucketURL: 'gs://healt-app.appspot.com/'
};